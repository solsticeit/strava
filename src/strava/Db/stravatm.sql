USE [master]
GO
/****** Object:  Database [Strava]    Script Date: 04/10/2021 23:02:18 ******/
CREATE DATABASE [Strava]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Strava', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Strava.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Strava_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Strava_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Strava] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Strava].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Strava] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Strava] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Strava] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Strava] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Strava] SET ARITHABORT OFF 
GO
ALTER DATABASE [Strava] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Strava] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Strava] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Strava] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Strava] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Strava] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Strava] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Strava] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Strava] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Strava] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Strava] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Strava] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Strava] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Strava] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Strava] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Strava] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Strava] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Strava] SET RECOVERY FULL 
GO
ALTER DATABASE [Strava] SET  MULTI_USER 
GO
ALTER DATABASE [Strava] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Strava] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Strava] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Strava] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Strava] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Strava] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Strava', N'ON'
GO
ALTER DATABASE [Strava] SET QUERY_STORE = OFF
GO
USE [Strava]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[Id] [bigint] NOT NULL,
	[ResourceState] [int] NOT NULL,
	[AthleteId] [bigint] NOT NULL,
	[ExternalId] [nvarchar](max) NULL,
	[UploadId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Distance] [float] NOT NULL,
	[MovingTime] [nvarchar](max) NULL,
	[ElapsedTime] [bigint] NOT NULL,
	[TotalElevationGain] [float] NOT NULL,
	[Type] [nvarchar](max) NULL,
	[StartDate] [datetime] NULL,
	[StartDateLocal] [varchar](50) NULL,
	[Timezone] [nvarchar](max) NULL,
	[LocationCity] [nvarchar](max) NULL,
	[LocationState] [nvarchar](max) NULL,
	[LocationCountry] [nvarchar](max) NULL,
	[StartLatitude] [float] NOT NULL,
	[StartLongitude] [float] NOT NULL,
	[AchievementCount] [bigint] NOT NULL,
	[KudosCount] [bigint] NOT NULL,
	[CommentCount] [bigint] NOT NULL,
	[AthleteCount] [bigint] NOT NULL,
	[PhotoCount] [bigint] NOT NULL,
	[Trainer] [bit] NOT NULL,
	[Commute] [bit] NOT NULL,
	[Manual] [bit] NOT NULL,
	[Private] [bit] NOT NULL,
	[Flagged] [bit] NOT NULL,
	[AverageSpeed] [float] NOT NULL,
	[MaxSpeed] [float] NOT NULL,
	[AverageWatts] [float] NOT NULL,
	[Kilojoules] [float] NOT NULL,
	[DeviceWatts] [bit] NOT NULL,
	[HasKudoed] [bit] NOT NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ActivityStats]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityStats](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BiggestRideDistance] [real] NOT NULL,
	[BiggestClimbElevationGain] [real] NOT NULL,
	[AthleteId] [bigint] NOT NULL,
 CONSTRAINT [PK_ActivityStats] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Athlete]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Athlete](
	[Id] [bigint] NOT NULL,
	[ResourceState] [int] NOT NULL,
	[ProfileMedium] [nvarchar](max) NULL,
	[Profile] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Athlete] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AthleteMetrics]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AthleteMetrics](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AthleteId] [bigint] NOT NULL,
	[Count] [bigint] NOT NULL,
	[Distance] [real] NOT NULL,
	[MovingTime] [real] NOT NULL,
	[ElapsedTime] [real] NOT NULL,
	[ElevationGain] [real] NOT NULL,
	[AchievementCount] [bigint] NOT NULL,
	[Type] [nvarchar](max) NULL,
 CONSTRAINT [PK_AthleteMetrics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapUrls]    Script Date: 04/10/2021 23:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapUrls](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActivityId] [bigint] NOT NULL,
	[Url] [nvarchar](max) NULL,
 CONSTRAINT [PK_MapUrls] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [Strava] SET  READ_WRITE 
GO
