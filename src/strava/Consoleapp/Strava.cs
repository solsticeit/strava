﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using StravaDownloader.Model;
namespace StravaDownloader
{
    internal class Strava
    {
       
        private readonly TokenHandler _tokenHandler ;
        public Strava(int clientId, string clientSecret,string code, bool isNewToken)
        {
            _tokenHandler = new TokenHandler(clientId, clientSecret, isNewToken);
            _tokenHandler.GetToken(code);
        }

       
        private  T GetStravaData<T>(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (WebResponse response =  request.GetResponseAsync().Result)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            string result =  reader.ReadToEndAsync().Result;
                            return JsonConvert.DeserializeObject<T>(result);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                using (WebResponse errorResponse = ex.Response)
                {
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8")))
                        {
                            string errorText = reader.ReadToEnd();
                        }
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public  IEnumerable<Activity> GetActivities()
        {
            try
            {
                string url = string.Format("https://www.strava.com/api/v3/athlete/activities?access_token={0}&per_page=200", _tokenHandler.AccessToken);
                return  this.GetStravaData<IEnumerable<Activity>>(url);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public ActivityStats GetAthleteStats(long athleteId)
        {
            try
            {

               
                string url = string.Format("https://www.strava.com/api/v3/athletes/{1}/stats?access_token={0}&per_page=200", _tokenHandler.AccessToken, athleteId);
                return this.GetStravaData<ActivityStats>(url);
            }
            catch (Exception )
            {
                return new ActivityStats();
            }
        }
        public Athlete GetAthlete()
        {
            string url = string.Format("https://www.strava.com/api/v3/athlete?access_token={0}&per_page=200", _tokenHandler.AccessToken);
            return  this.GetStravaData<Athlete>(url);
        }
    }
}
