﻿using AutoMapper;

using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StravaDownloader.EF;
using System;

using System.IO;
using System.Linq;
using StravaDownloader.Dto;
using StravaDownloader.Model;
using StravaDownloader.Helper;

namespace StravaDownloader
{
    class Program
    {

        private static User[] _users;
        private static int _clientId;
        private static string  _clientSecret;
        private static string _googleMapsAPIKey;

        private static SqlContext _context;
        public static void LoadJson(string file)
        {
            using (StreamReader r = new StreamReader(file))
            {
                string json = r.ReadToEnd();
                var a = JsonConvert.DeserializeObject<Users>(json);
                _users = a.UserList;
            }
        }
        private static void GetSettings()
        {
            var builder = new ConfigurationBuilder()
 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);


            var b = builder.Build();

            var section = b.GetSection("ApplicationSettings");



             _clientId = Convert.ToInt32(section["ClientId"]); //70987;
             _clientSecret = Convert.ToString(section["ClientSecret"]);/// "5b84ec5a938bc708fb7d8df1196e89165230406a";
            string usersFile = Convert.ToString(section["UsersFile"]);

           _googleMapsAPIKey = Convert.ToString(section["GoogleMapsApiKey"]);
            LoadJson(usersFile);
        }
        private static void TruncateTables()
        {

            var dh = new DatabaseHelper(_context.ConnectionString);
            dh.ExecuteSql("truncate table dbo.[Activity]");
            dh.ExecuteSql("truncate table dbo.[Athlete]");
            dh.ExecuteSql("truncate table dbo.[ActivityStats]");
            dh.ExecuteSql("truncate table dbo.[AthleteMetrics]");
            dh.ExecuteSql("truncate table dbo.[MapUrls]");
        }

        static void SaveAthleteMetrics(Mapper mapper,ActivityStats stats, long athleteId)
        {

            var allRideTotals = mapper.Map<AthleteMetricsDto>(stats.AllRideTotals);
            allRideTotals.AthleteId = athleteId;
            allRideTotals.Type = "AllRideTotals";
            _context.Add(allRideTotals);
            _context.SaveChanges();

            var allRunTotals = mapper.Map<AthleteMetricsDto>(stats.AllRunTotals);
            allRunTotals.AthleteId = athleteId;
            allRunTotals.Type = "AllRunTotals";
            _context.Add(allRunTotals);
            _context.SaveChanges();

            var recentRideTotals = mapper.Map<AthleteMetricsDto>(stats.RecentRideTotals);
            recentRideTotals.AthleteId = athleteId;
            recentRideTotals.Type = "RecentRideTotals";
            _context.Add(recentRideTotals);
            _context.SaveChanges();

            var recentRunTotals = mapper.Map<AthleteMetricsDto>(stats.RecentRunTotals);
            recentRunTotals.AthleteId = athleteId;
            recentRunTotals.Type = "RecentRunTotals";
            _context.Add(recentRunTotals);
            _context.SaveChanges();

            var recentSwimTotals = mapper.Map<AthleteMetricsDto>(stats.RecentSwimTotals);
            recentSwimTotals.AthleteId = athleteId;
            recentSwimTotals.Type = "RecentSwimTotals";
            _context.Add(recentSwimTotals);
            _context.SaveChanges();

            var ytdRideTotals = mapper.Map<AthleteMetricsDto>(stats.YtdRideTotals);
            ytdRideTotals.AthleteId = athleteId;
            ytdRideTotals.Type = "YtdRideTotals";
            _context.Add(ytdRideTotals);
            _context.SaveChanges();

            var ytdRunTotals = mapper.Map<AthleteMetricsDto>(stats.YtdRunTotals);
            ytdRunTotals.AthleteId = athleteId;
            ytdRunTotals.Type = "YtdRunTotals";
            _context.Add(ytdRunTotals);
            _context.SaveChanges();

            var ytdSwimTotals = mapper.Map<AthleteMetricsDto>(stats.YtdSwimTotals);
            ytdSwimTotals.AthleteId = athleteId;
            ytdSwimTotals.Type = "YtdSwimTotals";
            _context.Add(ytdSwimTotals);
            _context.SaveChanges();


        }
        static void Main(string[] args)
        {

            GetSettings();

            var mapper = Config.AutoMapper.GetMapper();
            _context = new SqlContext();
           

            TruncateTables();


            foreach (var user in _users)
            {
               
                var strava = new Strava(_clientId, _clientSecret, user.RefreshToken, false);

                var athlete = strava.GetAthlete();


                var stats = strava.GetAthleteStats(athlete.Id.Value);
                var statsDto = mapper.Map<ActivityStatsDto>(stats);
                statsDto.AthleteId = athlete.Id.Value;
                _context.Add(statsDto);
                _context.SaveChanges();

                SaveAthleteMetrics(mapper, stats, athlete.Id.Value);
                Console.WriteLine("##########################################################################################################");
                Console.WriteLine($"Athlete:{user.Name}");
                Console.WriteLine($"All Run Totals:{stats.AllRunTotals.Count}");
                Console.WriteLine($"All Ride Totals:{stats.AllRideTotals.Count}");
                Console.WriteLine($"Biggest Ride Distance:{stats.BiggestRideDistance} m");
                Console.WriteLine($"Ytd Run Totals:{stats.YtdRunTotals.Count}");
                Console.WriteLine("##########################################################################################################");
                var activities = strava.GetActivities().ToArray();

                if (activities != null && activities.Length > 0)
                {
                    var dtox = mapper.Map<AthleteDto>(activities[0].Athlete);
                    dtox.Name = user.Name;
                    dtox.FullName = user.Name + " (" + athlete.Id.Value + ")";

                   
                    _context.Add(dtox);
                    _context.SaveChanges();
                    foreach (var activity in activities)
                    {
                        if (!string.IsNullOrEmpty(activity?.Map?.PolyLine))
                        {
                            //var points = GooglePoints.Decode(activity.Map.PolyLine);

                            //var outPut = "https://www.bing.com/maps?sp=Polyline.";
                            //var counter = 0;

                            //foreach (var point in points)
                            //{
                            //    if (counter % 3==0)

                            //                 outPut += point.Latitude + "_" + point.Longitude + "_";
                            //    counter++;
                            //}
                            //outPut += activity.Name + " (" + activity.Id + ")";
                          
                            var url =@"https://maps.googleapis.com/maps/api/staticmap?size=500x450&path=weight:3%7Ccolor:black%7Cenc:"+ activity.Map.PolyLine + "&key="+ _googleMapsAPIKey;
                            var mapUrlDto = new MapUrlDto() { ActivityId = activity.Id.Value, Url = url };
                            _context.Add(mapUrlDto);
                            _context.SaveChanges();
                        }
                        var dto = mapper.Map<ActivityDto>(activity);
                      
                     
                        dto.AthleteId = activity.Athlete.Id.Value;
                        _context.Add(dto);
                        _context.SaveChanges();
                        Console.WriteLine($"Date:{activity.Date}\tName:{activity.Name}\tType:{activity.Type}\tDistance:{Math.Round(activity.Distance.Value, 2)} km\tMoving Time:{activity.MovingTime}\tAvg Speed:{Math.Round(activity.AverageSpeed, 2)} km/h\tMax Speed:{Math.Round(activity.MaxSpeed.Value, 2)} km/h.");
                    }
                }
          

            }
            Console.WriteLine("Success");
            Console.ReadLine();
        }
    }
}
