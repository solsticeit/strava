﻿using System.Collections.Generic;

namespace StravaDownloader
{
    internal class TokenHandler
    {
        private readonly int _clientId;
        private readonly string _clientSecret;
       
        private readonly bool _isNewToken;
        internal string AccessToken { get; private set; }
        internal  TokenHandler(int clientId, string clientSecret, bool isNewToken)
        {
            _clientId = clientId;
            _clientSecret = clientSecret;
          
            _isNewToken = isNewToken;
            

        }
        private string GetTokenFromRefreshToken(string refreshToken)
        {
            var value = new Dictionary<string, string>
         {
            { "client_id", _clientId.ToString() },
            { "client_secret", _clientSecret },
             //{ "code", "22d5bbfdf477a887b7e73643b26e5a121d6a3b94" },
            { "refresh_token",refreshToken},
            { "grant_type", "refresh_token" }
         };
            var client = new System.Net.Http.HttpClient();
            var content = new System.Net.Http.FormUrlEncodedContent(value);
            var result2 = client.PostAsync("https://www.strava.com/oauth/token", content).Result;
            var resultContent = result2.Content.ReadAsStringAsync().Result;
            var stravaDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<StravaRoot>(resultContent);
            return stravaDetails.access_token;
        }
      

        private string GetRefreshToken(string code)
        {
            var value = new Dictionary<string, string>
         {
            { "client_id", _clientId.ToString() },
            { "client_secret", _clientSecret },
             { "code",code}, //this is a one time code 
            //{ "refresh_token",_refreshToken},
            { "grant_type", "authorization_code" }
         };

            /*
             * put url in browser then capture code when the browser redirects
             * 
             * 
             * https://www.strava.com/oauth/authorize?client_id=70987&redirect_uri=http://localhost&response_type=code&scope=activity:read_all*/
            var client = new System.Net.Http.HttpClient();
            var content = new System.Net.Http.FormUrlEncodedContent(value);
            var result2 = client.PostAsync("https://www.strava.com/oauth/token", content).Result;
            string resultContent = result2.Content.ReadAsStringAsync().Result;
            var stravaDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<StravaRoot>(resultContent);
            return stravaDetails.refresh_token;
        }


        internal string GetToken(string code)
        {

            //            1) Get authorization code from authorization page. This is a one time, manual step. 
            //Paste the below code in a browser, hit enter then grab the "code" part from the resulting url.

            //https://www.strava.com/oauth/authorize?client_id=70987&redirect_uri=http://localhost&response_type=code&scope=activity:read_all

            //2) Exchange authorization code for access token &refresh token

            //https://www.strava.com/oauth/token?client_id=your_client_id&client_secret=your_client_secret&code=your_code_from_previous_step&grant_type=authorization_code

            //3) View your activities using the access token just received

            //https://www.strava.com/api/v3/athlete/activities?access_token=access_token_from_previous_step

            //3) Use refresh token to get new access tokens

            //https://www.strava.com/oauth/token?client_id=your_client_id&client_secret=your_client_secret&refresh_token=your_refresh_token_from_previous_step&grant_type=refresh_token


         

                if (_isNewToken)
                {
                    var token = GetRefreshToken(code);

                    AccessToken = GetTokenFromRefreshToken(token);
                }

                else

                    AccessToken = GetTokenFromRefreshToken(code);
            
            return AccessToken;

        }
    }
}
