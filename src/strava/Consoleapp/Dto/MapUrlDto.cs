﻿using Newtonsoft.Json;
using StravaDownloader.Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace StravaDownloader.Dto
{

    [Table("MapUrls")]
    public class MapUrlDto
    {
        [Key]
        public long Id { get; set; }

        public long ActivityId { get; set; }

        public string Url { get; set; }

    }
}
