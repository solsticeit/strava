﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace StravaDownloader.Dto
{
    [Table("Activity")]
    public class ActivityDto
    {
        //[Key]
        //public MapDto Map { get; set; }
        public long Id { get; set; }

     
        public int ResourceState { get; set; }
        public long AthleteId { get; set; }


        public string ExternalId { get; set; }

        public long UploadId { get; set; }


        //public AthleteDto Athlete { get; set; }


        public string Name { get; set; }

        private double _distance;

      
        public double Distance
        {
            get
            {
                return _distance / 1000;
            }
            set
            {
                _distance = value;
            }
        }

        //private long _movingTime = 0;

       
        public string MovingTime { get; set; }
        //{
        //    get
        //    {
        //        if (_movingTime != null)
        //        {
        //            TimeSpan ts = TimeSpan.FromSeconds((int)_movingTime);
        //            return ts.ToString();
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    set
        //    {
        //        _movingTime = int.Parse(value);
        //    }
        //}


        public long ElapsedTime { get; set; }

   
        public double TotalElevationGain { get; set; }

      
        public string Type { get; set; }

    
        public string StartDate { get; set; }

    
        public string StartDateLocal { get; set; }

     
        public string Timezone { get; set; }

        
        //public double[] StartLatlng { get; set; }

      
        //public double[] EndLatlng { get; set; }


        public string LocationCity { get; set; }

        
        public string LocationState { get; set; }

        
        public string LocationCountry { get; set; }

       
        public double StartLatitude { get; set; }

       
        public double StartLongitude { get; set; }

        
        public long AchievementCount { get; set; }

        
        public long KudosCount { get; set; }

        
        public long CommentCount { get; set; }

       
        public long AthleteCount { get; set; }

        
        public long PhotoCount { get; set; }

        
        public bool Trainer { get; set; }

        
        public bool Commute { get; set; }

       
        public bool Manual { get; set; }

       
        public bool Private { get; set; }

        
        public bool Flagged { get; set; }


        //public object GearId { get; set; }

        private double _averageSpeed;
        //[JsonProperty("average_speed")]
        public double AverageSpeed
        {
            get
            {
                return _averageSpeed != null ? (double)_averageSpeed * 3.6 : 0;
            }
            set
            {
                _averageSpeed = value;
            }
        }

        private double _maxSpeed;

        //[JsonProperty("max_speed")]
        public double MaxSpeed
        {
            get
            {
                return _maxSpeed != null ? _maxSpeed * 3.6 : 0;
            }
            set
            {
                _maxSpeed = value;
            }
        }


        public double AverageWatts { get; set; }

    
        public double Kilojoules { get; set; }

      
        public bool DeviceWatts { get; set; }

    
        //public object Truncated { get; set; }

     
        public bool HasKudoed { get; set; }

        public DateTime Date
        {
            get
            {
                return DateTime.Parse(this.StartDate);
            }
        }
    }
}