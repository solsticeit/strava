﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StravaDownloader.Dto
{
    [Table("AthleteMetrics")]
    public class AthleteMetricsDto
    {
        //[Key]
        public long AthleteId { get; set; }
        public long Id { get; set; }
     
        public long Count { get; set; }
        
        public float Distance { get; set; }
       
        public float MovingTime { get; set; }
      
        public float ElapsedTime { get; set; }
       
        public float ElevationGain { get; set; }

      
        public long AchievementCount { get; set; }
        public string Type { get; set; }
    }
}

