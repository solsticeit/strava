﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StravaDownloader.Dto
{
    [Table("Athlete")]
    public class AthleteDto
    {
        //[Key]
      
        public long Id { get; set; }

       
        public int ResourceState { get; set; }

      
        public string ProfileMedium { get; set; }

       
        public string Profile { get; set; }

        public string Name { get; set; }


        public string FullName { get; set; }
    }
}