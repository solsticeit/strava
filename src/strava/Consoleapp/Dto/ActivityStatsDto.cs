﻿

using Newtonsoft.Json;
using StravaDownloader.Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StravaDownloader.Dto
{
    [Table("ActivityStats")]
    public class ActivityStatsDto
    {
        //[Key]
        //[JsonProperty("id")]
        public long Id { get; set; }
      
        public float BiggestRideDistance { get; set; }
        
        public float BiggestClimbElevationGain { get; set; }

        public long AthleteId { get; set; }


        

        //public AthleteMetricsDto RecentRideTotals { get; set; }

        //public AthleteMetricsDto AllRideTotals { get; set; }



        //public AthleteMetricsDto RecentRunTotals { get; set; }


        //public AthleteMetricsDto AllRunTotals { get; set; }


        //public AthleteMetricsDto RecentSwimTotals { get; set; }



        //public AthleteMetricsDto YtdRideTotals { get; set; }


        //public AthleteMetricsDto YtdRunTotals { get; set; }


        //public AthleteMetricsDto YtdSwimTotals { get; set; }

    }
}
