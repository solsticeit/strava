﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using StravaDownloader.EF;

namespace StravaDownloader.Migrations
{
    [DbContext(typeof(SqlContext))]
    [Migration("20211004214144_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("StravaDownloader.Dto.ActivityDto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("AchievementCount")
                        .HasColumnType("bigint");

                    b.Property<long>("AthleteCount")
                        .HasColumnType("bigint");

                    b.Property<long>("AthleteId")
                        .HasColumnType("bigint");

                    b.Property<double>("AverageSpeed")
                        .HasColumnType("float");

                    b.Property<double>("AverageWatts")
                        .HasColumnType("float");

                    b.Property<long>("CommentCount")
                        .HasColumnType("bigint");

                    b.Property<bool>("Commute")
                        .HasColumnType("bit");

                    b.Property<bool>("DeviceWatts")
                        .HasColumnType("bit");

                    b.Property<double>("Distance")
                        .HasColumnType("float");

                    b.Property<long>("ElapsedTime")
                        .HasColumnType("bigint");

                    b.Property<string>("ExternalId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Flagged")
                        .HasColumnType("bit");

                    b.Property<bool>("HasKudoed")
                        .HasColumnType("bit");

                    b.Property<double>("Kilojoules")
                        .HasColumnType("float");

                    b.Property<long>("KudosCount")
                        .HasColumnType("bigint");

                    b.Property<string>("LocationCity")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LocationCountry")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LocationState")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Manual")
                        .HasColumnType("bit");

                    b.Property<double>("MaxSpeed")
                        .HasColumnType("float");

                    b.Property<string>("MovingTime")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("PhotoCount")
                        .HasColumnType("bigint");

                    b.Property<bool>("Private")
                        .HasColumnType("bit");

                    b.Property<int>("ResourceState")
                        .HasColumnType("int");

                    b.Property<string>("StartDate")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("StartDateLocal")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("StartLatitude")
                        .HasColumnType("float");

                    b.Property<double>("StartLongitude")
                        .HasColumnType("float");

                    b.Property<string>("Timezone")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("TotalElevationGain")
                        .HasColumnType("float");

                    b.Property<bool>("Trainer")
                        .HasColumnType("bit");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("UploadId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.ToTable("Activity");
                });

            modelBuilder.Entity("StravaDownloader.Dto.ActivityStatsDto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("AthleteId")
                        .HasColumnType("bigint");

                    b.Property<float>("BiggestClimbElevationGain")
                        .HasColumnType("real");

                    b.Property<float>("BiggestRideDistance")
                        .HasColumnType("real");

                    b.HasKey("Id");

                    b.ToTable("ActivityStats");
                });

            modelBuilder.Entity("StravaDownloader.Dto.AthleteDto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FullName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Profile")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ProfileMedium")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ResourceState")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Athlete");
                });

            modelBuilder.Entity("StravaDownloader.Dto.AthleteMetricsDto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("AchievementCount")
                        .HasColumnType("bigint");

                    b.Property<long>("AthleteId")
                        .HasColumnType("bigint");

                    b.Property<long>("Count")
                        .HasColumnType("bigint");

                    b.Property<float>("Distance")
                        .HasColumnType("real");

                    b.Property<float>("ElapsedTime")
                        .HasColumnType("real");

                    b.Property<float>("ElevationGain")
                        .HasColumnType("real");

                    b.Property<float>("MovingTime")
                        .HasColumnType("real");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("AthleteMetrics");
                });

            modelBuilder.Entity("StravaDownloader.Dto.MapUrlDto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("ActivityId")
                        .HasColumnType("bigint");

                    b.Property<string>("Url")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("MapUrls");
                });
#pragma warning restore 612, 618
        }
    }
}
