﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace StravaDownloader
{
    internal class DatabaseHelper
    {
        internal string ConnectionString { get; }
        internal DatabaseHelper(string connectionString)
        {
            ConnectionString = connectionString;
        }
        internal void ExecuteSql(string sql)
        {
            using (var sqlConn = new SqlConnection(ConnectionString))

            {
                sqlConn.Open();
                using (var sqlCmd = sqlConn.CreateCommand())
                {
                    sqlCmd.CommandText = sql;
                    sqlCmd.ExecuteNonQuery( );
                }
                sqlConn.Close();

            }
        }
    }
}
