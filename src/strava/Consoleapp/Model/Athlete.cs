﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace StravaDownloader.Model
{
    public class Athlete
    {
        [Key]
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("resource_state")]
        public int? ResourceState { get; set; }

        [JsonProperty("profile_medium")]
        public string ProfileMedium { get; set; }

        [JsonProperty("profile")]
        public string Profile { get; set; }
    }
}