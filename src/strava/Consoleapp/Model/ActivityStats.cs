﻿

using Newtonsoft.Json;
using StravaDownloader.Model;
using System.ComponentModel.DataAnnotations;

namespace StravaDownloader.Model
{
    //[JsonObject]
    public class ActivityStats
    {
        [Key]
        //[JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("biggest_ride_distance")]
      
        public float? BiggestRideDistance { get; set; }
        [JsonProperty("biggest_climb_elevation_gain")]
        public float? BiggestClimbElevationGain { get; set; }


        [JsonProperty("recent_ride_totals")]
        
        public AthleteMetrics RecentRideTotals { get; set; }
        [JsonProperty("all_ride_totals")]
        public AthleteMetrics AllRideTotals { get; set; }


        [JsonProperty("recent_run_totals")]
        public AthleteMetrics RecentRunTotals { get; set; }

        [JsonProperty("all_run_totals")]
        public AthleteMetrics AllRunTotals { get; set; }

        [JsonProperty("recent_swim_totals")]
        public AthleteMetrics RecentSwimTotals { get; set; }


        [JsonProperty("ytd_ride_totals")]
        public AthleteMetrics YtdRideTotals { get; set; }

        [JsonProperty("ytd_run_totals")]
        public AthleteMetrics YtdRunTotals { get; set; }

        [JsonProperty("ytd_swim_totals")]
        public AthleteMetrics YtdSwimTotals { get; set; }

    }
}
