﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace StravaDownloader.Model
{
    [JsonObject]
    public class AthleteMetrics
    {
        [Key]
        //[JsonProperty("id")]
        public long? Id { get; set; }
        //"count":0,"distance":0.0,"moving_time":0,"elapsed_time":0,"elevation_gain":0.0,"achievement_count":0
        [JsonProperty("count")]
        public long? Count { get; set; }
        [JsonProperty("distance")]
        public float? Distance { get; set; }
        [JsonProperty("moving_time")]
        public float? MovingTime { get; set; }
        [JsonProperty("elapsed_time")]
        public float? ElapsedTime { get; set; }
        [JsonProperty("elevation_gain")]
        public float? ElevationGain { get; set; }

        [JsonProperty("achievement_count")]
        public long? AchievementCount { get; set; }

    }
}

