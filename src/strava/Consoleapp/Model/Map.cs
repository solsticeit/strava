﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
namespace StravaDownloader.Model
{
    [JsonObject]
    public class Map
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("summary_polyline")]
        public string PolyLine { get; set; }

        //      	"map": {
        //	"id": "a3521223810",
        //	"summary_polyline": "qyptI|qpR`@_@Tc@KaCS}BMaACeAg@}DC_@BSy@uDYeA?Sh@c@\\i@R{@C_A\\{BK_AUw@Uk@e@LY^gHvCmA~@a@^[r@a@b@c@Xg@Re@Fc@\\e@R_@Zg@JgA`@eAv@a@NcAn@g@@c@Lg@Da@Re@JiAf@e@Nc@H_@l@a@Xe@JgAh@c@JgAJe@?eAXe@Vg@Da@PgAv@c@Je@Fc@ZmATsBd@c@^iAZe@Jc@BoB`@iALg@Ra@b@iBhAe@?e@Da@JyFrBc@j@kAPe@NeAp@e@Bg@Lc@RuCnBw@tAWp@Ut@EhAWlC[bC@pAIbA@BEA@aAD_AH_ADgA?cCPw@\\a@h@qBZm@z@mA`@[Ze@hByAf@Hf@If@Eb@Jd@Y^_@dAq@d@OhG_Cf@KjA_@b@]d@U`@_@^m@jAuCTy@f@aE\\eEZyBb@yB\\oCv@mDj@kBhBgEn@w@\\m@r@}A~@kA^o@`@c@NI\\b@\\h@\\n@Vn@tBjDvApCVn@t@zAhB|DZl@h@fBt@fB\\n@\\d@f@V`@X`@Ff@?b@Cf@KnBs@b@[`@Sf@MjBaAb@a@fAw@^]\\c@`@U`@_@`@W~IyDZdAPz@B`AO~@CbA?bA}@lAUZRx@@`AbAvFH`AJz@NbCV~BF`A@`AYl@c@\\m@Z",
        //	"resource_state": 2
        //},
    }
}
