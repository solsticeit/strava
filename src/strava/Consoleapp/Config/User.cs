﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StravaDownloader
{
    public class User
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
