﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StravaDownloader
{
   public class Users
    {
        [JsonProperty("Users")]
        public User[] UserList { get; set; }
    }
}
