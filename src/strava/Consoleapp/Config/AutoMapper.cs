﻿using AutoMapper;
using StravaDownloader.Dto;
using StravaDownloader.Model;


namespace StravaDownloader.Config
{
    internal static class AutoMapper
    {


        internal static Mapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Activity, ActivityDto>();
                cfg.CreateMap<Athlete, AthleteDto>();
                cfg.CreateMap<ActivityStats, ActivityStatsDto>();
                cfg.CreateMap<AthleteMetrics, AthleteMetricsDto>();
                //cfg.CreateMap<Map, MapDto>();


            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
