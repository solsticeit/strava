﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StravaDownloader.Dto;

namespace StravaDownloader.EF
{
    public class SqlContext : DbContext
    {
        public DbSet<AthleteDto> Athlete { get; set; }
        public DbSet<ActivityDto> Activity { get; set; }
        public DbSet<ActivityStatsDto> ActivityStats { get; set; }

        public DbSet<AthleteMetricsDto> ActivityMetrics { get; set; }

        public DbSet<MapUrlDto> MapUrls { get; set; }
        public string ConnectionString { get; private set; } = @"Data Source=.;Initial Catalog=Strava;Integrated Security=true";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseSqlServer(ConnectionString);

            optionsBuilder.EnableSensitiveDataLogging(true);

        }
    }
}
