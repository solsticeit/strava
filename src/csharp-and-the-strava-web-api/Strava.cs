﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace csharp_and_the_strava_web_api
{
    class Strava
    {
        private readonly string _accessToken;
        private readonly int _clientId = 70987;
        private readonly string _clientSecret = "5b84ec5a938bc708fb7d8df1196e89165230406a";

        //private readonly string _exhangeCode = "5826fd77dd68ac67e63ecb98c8f8377d58ad1ff0";

        //private readonly string _refreshToken = "b7cb5a347a1c5574d9c829f2954c3bfd27386aba";
        public Strava(string accessToken)
        {
            _accessToken = accessToken;
        }

        private string GetNewToken()
        {
            var value = new Dictionary<string, string>
         {
            { "client_id", _clientId.ToString() },
            { "client_secret", _clientSecret },
             { "code", "b75d116dc8048287e5e9a020f25b7407934dbda6" }, //this is a one time code 
            //{ "refresh_token",_refreshToken},
            { "grant_type", "authorization_code" }
         };

            /*
             * put url in browser then capture code when the browser redirects
             * 
             * 
             * https://www.strava.com/oauth/authorize?client_id=70987&redirect_uri=http://localhost&response_type=code&scope=activity:read_all*/
            var client = new System.Net.Http.HttpClient();
            var content = new System.Net.Http.FormUrlEncodedContent(value);
            var result2 = client.PostAsync("https://www.strava.com/oauth/token", content).Result;
            string resultContent = result2.Content.ReadAsStringAsync().Result;
            var stravaDetails = JsonConvert.DeserializeObject<StravaRoot>(resultContent);
            return stravaDetails.access_token;
        }
        private void GetData(string accessToken)
        {
            var url = string.Format("https://www.strava.com/api/v3/athlete/activities?access_token={0}per_page=200", accessToken);
            var myUri = new Uri(url);
            var myWebRequest = WebRequest.Create(myUri);
            var myHttpWebRequest = (HttpWebRequest)myWebRequest;
            myHttpWebRequest.PreAuthenticate = true;
            myHttpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);
            myHttpWebRequest.Accept = "application/json";

            var myWebResponse = myWebRequest.GetResponse();
            var responseStream = myWebResponse.GetResponseStream();
            //if (responseStream == null) return null;

            var myStreamReader = new StreamReader(responseStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();

            responseStream.Close();
            myWebResponse.Close();

        }
        private string GetTokenFromRefreshToken(string refreshToken)
        {
           var value = new Dictionary<string, string>
         {
            { "client_id", _clientId.ToString() },
            { "client_secret", _clientSecret },
             //{ "code", "22d5bbfdf477a887b7e73643b26e5a121d6a3b94" },
            { "refresh_token",refreshToken},
            { "grant_type", "refresh_token" }
         };
           var client = new System.Net.Http.HttpClient();
            var content = new System.Net.Http.FormUrlEncodedContent(value);
            var result2 = client.PostAsync("https://www.strava.com/oauth/token", content).Result;
            var resultContent = result2.Content.ReadAsStringAsync().Result;
            var stravaDetails = JsonConvert.DeserializeObject<StravaRoot>(resultContent);
            return stravaDetails.access_token;
        }
        public void Exchange()
        {

            //            1) Get authorization code from authorization page. This is a one time, manual step. 
            //Paste the below code in a browser, hit enter then grab the "code" part from the resulting url.

            //https://www.strava.com/oauth/authorize?client_id=70987&redirect_uri=http://localhost&response_type=code&scope=activity:read_all

            //2) Exchange authorization code for access token &refresh token

            //https://www.strava.com/oauth/token?client_id=your_client_id&client_secret=your_client_secret&code=your_code_from_previous_step&grant_type=authorization_code

            //3) View your activities using the access token just received

            //https://www.strava.com/api/v3/athlete/activities?access_token=access_token_from_previous_step

            //3) Use refresh token to get new access tokens

            //https://www.strava.com/oauth/token?client_id=your_client_id&client_secret=your_client_secret&refresh_token=your_refresh_token_from_previous_step&grant_type=refresh_token


            try
            {

                var accessToken = "5e7ac1681f94b952d19db6189e9d94ab0ec0c1b9";
                GetData(accessToken);
                //var token = GetNewToken();
                //GetData(token);

                //var refreshToken = "b5d0d41b90943a968a0f055036fe281a5c3ad42e";
                //var token = GetTokenFromRefreshToken(refreshToken);
                //GetData(token);
            }
            catch
            {
                try
                {
                    var refreshToken = "b5d0d41b90943a968a0f055036fe281a5c3ad42e";
                    var token = GetTokenFromRefreshToken(refreshToken);
                    GetData(token);
                } catch
                {
                    var token = GetNewToken();
                    GetData(token);
                }
            }

       

        }
        private  T GetStravaData<T>(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (WebResponse response =  request.GetResponseAsync().Result)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            string result =  reader.ReadToEndAsync().Result;
                            return JsonConvert.DeserializeObject<T>(result);
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                using (WebResponse errorResponse = ex.Response)
                {
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8")))
                        {
                            string errorText = reader.ReadToEnd();
                        }
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public  IEnumerable<Activity> GetStravaRides()
        {
            try
            {
                string url = string.Format("https://www.strava.com/api/v3/athlete/activities?access_token={0}&per_page=200", _accessToken);
                return  this.GetStravaData<IEnumerable<Activity>>(url);
            }
            catch (Exception ex)
            {
                return new List<Activity>();
            }
        }
        public ActivityStats GetAthleteStats()
        {
            try
            {

                var id = 2550812;
                string url = string.Format("https://www.strava.com/api/v3/athletes/{1}/stats?access_token={0}&per_page=200", _accessToken,id);
                return this.GetStravaData<ActivityStats>(url);
            }
            catch (Exception ex)
            {
                return new ActivityStats();
            }
        }
        public Athlete GetAthlete()
        {
            string url = string.Format("https://www.strava.com/api/v3/athlete?access_token={0}&per_page=200", _accessToken);
            return  this.GetStravaData<Athlete>(url);
        }
    }
}
