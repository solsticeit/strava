﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;

namespace csharp_and_the_strava_web_api
{
    //[JsonObject]
    public class ActivityStats
    {
        //{"biggest_ride_distance":65162.2,
        //"biggest_climb_elevation_gain":382.0,
        //"recent_ride_totals":{"count":0,"distance":0.0,"moving_time":0,"elapsed_time":0,"elevation_gain":0.0,"achievement_count":0},
        //"all_ride_totals":{"count":67,"distance":1142287,"moving_time":242030,"elapsed_time":349901,"elevation_gain":8317},
        //"recent_run_totals":{"count":8,"distance":33954.500244140625,"moving_time":13282,"elapsed_time":14006,"elevation_gain":88.63538932800293,"achievement_count":0},
        //"all_run_totals":{"count":348,"distance":1719770,"moving_time":671830,"elapsed_time":1981688,"elevation_gain":10470},
        //"recent_swim_totals":        {"count":0,"distance":0.0,"moving_time":0,"elapsed_time":0,"elevation_gain":0.0,"achievement_count":0},"all_swim_totals":{"count":0,"distance":0,"moving_time":0,"elapsed_time":0,"elevation_gain":0},
        //"ytd_ride_totals":{"count":8,"distance":269870,"moving_time":60475,"elapsed_time":83061,"elevation_gain":1775},
        //"ytd_run_totals":{"count":87,"distance":474096,"moving_time":171606,"elapsed_time":290875,"elevation_gain":1637},
        //"ytd_swim_totals":{"count":0,"distance":0,"moving_time":0,"elapsed_time":0,"elevation_gain":0}}
        [JsonProperty("biggest_ride_distance")]
      
        public float BiggestRideDistance { get; set; }
        [JsonProperty("biggest_climb_elevation_gain")]
        public float BiggestClimbElevationGain { get; set; }

    }
}
