﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;



using System.Diagnostics;
//using com.strava.api.v3.Api;
//using com.strava.api.v3.Client;
//using com.strava.api.v3.Model;

namespace csharp_and_the_strava_web_api
{
    class Program
    {


        //static void et()
        //{

        //    // Configure OAuth2 access token for authorization: strava_oauth
        //    Configuration.Default.AccessToken = "YOUR_ACCESS_TOKEN";

        //    var apiInstance = new ActivitiesApi();
        //var id = 789;  // Long | The identifier of the activity.
        //var includeAllEfforts = true;  // Boolean | To include all segments efforts. (optional) 

        //    try
        //    {
        //        // Get Activity
        //        DetailedActivity result = apiInstance.getActivityById(id, includeAllEfforts);
        //Debug.WriteLine(result);
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.Print("Exception when calling ActivitiesApi.getActivityById: " + e.Message );
        //    }
        //}
         static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            Strava strava = new Strava("85b14d9b343201536fea2b91dbe8105c27df1459");

            strava.Exchange();
            strava.GetAthlete();
            var stats = strava.GetAthleteStats();
            IEnumerable<Activity> rides =  strava.GetStravaRides();

            foreach (Activity ride in rides)
            {
                Console.WriteLine($"On {ride.Date} you rode {ride.Distance} km with an average of {ride.AverageSpeed} km/h and max {ride.MaxSpeed} km/h.");
            }

            Console.ReadLine();
        }
    }
}
