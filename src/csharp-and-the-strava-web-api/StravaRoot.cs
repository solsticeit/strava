﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharp_and_the_strava_web_api
{
    public class StravaRoot
    {
        public string token_type { get; set; }
        public string access_token { get; set; }
        public int expires_at { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
    }
}
